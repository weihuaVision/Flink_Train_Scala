package flink.eureka;

import java.util.ArrayList;
import java.util.List;

/**
 * @author com.eureka.wh
 * @since 2019/6/22 17:02
 */
public class GenericDemo01 {

    public static void main(String[] args) {

        List<String> l1 = new ArrayList<String>();
        List<Integer> l2 = new ArrayList<Integer>();

        // 结果是true
//        System.out.println(l1.getClass() == l2.getClass());

        System.out.println("l1 = " + l1.getClass().getSimpleName());
        System.out.println("l2 = " + l2.getClass().getSimpleName());
    }
}
