package bitmap;

/**
 * @author com.eureka.wh
 * @since 2019/6/30 9:39
 */
public class BitMapTest {

    public static void main(String[] args) {

//        BitMap bitMap = new BitMap(128);
//        bitMap.setBit(126);
//        bitMap.setBit(75);
//        System.out.println(bitMap.getBit(126));
//        System.out.println(bitMap.getBit(78));


       bitOperation();
    }

    public static void bitOperation(){
        int a = 127; // 01111111
        int b = a >> 1; // 00111111
        System.out.println(b);

        int d = 127; // 01111111
        int e = d << 1; // 11111110
        System.out.println(e);
    }
}
