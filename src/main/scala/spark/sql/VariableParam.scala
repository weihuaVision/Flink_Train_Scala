package spark.sql

/**
  *
  * @author com.ymy.hadoop   
  * @since 2019/6/4 21:57
  */


object VariableParam  {

  def main(args: Array[String]): Unit = {

      // 可变参数可以不传
      println(sum())
//    println(sum(1 to 10:_*))
//    println(sum(1.to(10):_*))
//    println(sum(Array(1,2,3):_*))

//    showArr(Array("2","5"):_*)
  }

  def sum(numbers:Int*): Int ={
     var result = 0
     for(number <- numbers){
       result += number
     }
    result
  }

  def showArr(collections:Any*)={
     for(str <- collections){
//       if (str.isInstanceOf[String]){
//         val pStr = str.asInstanceOf[String]
//         println(pStr)
//       }
       println(str)
     }
  }
}




