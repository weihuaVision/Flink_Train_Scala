package spark.mgworkplace.test;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author com.ymy.hadoop
 * @since 2019/6/14 22:53
 */
public class MyTest {

    public static void main(String[] args) {

        Timestamp a = Timestamp.valueOf("2018-05-18 09:32:32");
        Timestamp b = Timestamp.valueOf("2015-05-18 09:32:32");
        Timestamp c = Timestamp.valueOf("2015-05-18 09:32:32");

        if (c.after(b) && c.before(a)){
            System.out.println("YES");
        }else {
            System.out.println("NO");
        }

//        System.out.println(a);

        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        System.out.println(dateFormat.format(a));

    }
}
