package spark.mgworkplace.test

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.Date

import spark.mgworkplace.job.udf.TransformKit.timeStampChangeRetStr
import spark.mgworkplace.job.udf._

/**
  *
  * @author com.ymy.hadoop   
  * @since 2019/6/14 23:17
  */
object MyScala {

  // 统计早、晚高峰7:00~10:00 行驶的总里程
  val beginDate = "2019-05-01 07:00:00"
  val endDate = "2019-05-31 23:59:59"
  val beginTime = "07:00:00"
  val endTime = "10:00:00"

  def main(args: Array[String]): Unit = {

    val a = Timestamp.valueOf(beginDate)
    println(timeStampChangeRetStr(a))

  }

  def test5()={

    val a = "07:00:00"
    val b = "10:00:00"

    println(b.substring(0,2).toInt -  a.substring(0,2).toInt )
  }

  def test4()={

    // 第一步是为了模拟SQL里面的TimeStamp类型
    // String ---> Timestamp
    val a = "2019-05-01 06:58:00"
    val b = "2019-05-01 10:00:00"

    val c = Timestamp.valueOf(a)
    val d = Timestamp.valueOf(b)

    // Timestamp ---> String
    val sdf = new SimpleDateFormat("HH:mm:ss")
    var start = sdf.format(c)
    var end = sdf.format(d)

    val sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var start2 = sdf2.format(c)
    var end2 = sdf2.format(d)

    println(start2)
    println(end2)

    if (start < "07:00:00"){
      start2 = new StringBuilder(start2).replace(11,19,"07:00:00").toString()
    }

    println("~~~~~~~~")
    println(start2)
    println(end2)


    //    val sdf = new SimpleDateFormat("HH:mm:ss")
    //    val timeStr = sdf.format(time)
    //    timeStr >= beginTime && timeStr <= endTime
  }


  def test3()={
    val a = Timestamp.valueOf(beginDate)
    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val timeStr = sdf.format(a)
    println(timeStr)
    println(timeStr >= beginDate && timeStr <= endDate)
  }


  def test2()={
    val a = Timestamp.valueOf(beginDate)
    val sdf = new SimpleDateFormat("HH:mm:ss")
    val timeStr = sdf.format(a)
    println(timeStr)
    println(timeStr >= beginTime && timeStr <= endTime)

  }

  def test()={
    //    val a = Timestamp.valueOf(beginTime)
    //
    //    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    //    val r = sdf.format(a)
    //    if (r > "05"){
    //      println("大")
    //    }
    //
    //    println(r)
  }

  // 获取当前时间
//  spark.udf.register("myTime",(mile:Timestamp)=>{
//    val now = new Timestamp(new Date().getTime)
//    now
//  })
}
