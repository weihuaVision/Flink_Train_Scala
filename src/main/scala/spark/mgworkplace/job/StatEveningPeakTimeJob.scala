package spark.mgworkplace.job

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession
import spark.mgworkplace.job.param.ParamKit._
import spark.mgworkplace.job.udf.UdfKit._

/**
  *
  * @author com.ymy.hadoop
  * @since 2019/6/16 9:19
  */
object StatEveningPeakTimeJob extends Logging with Job {

  def main(args: Array[String]): Unit = {

    logWarning("开始加载: "+ this.getClass.getSimpleName +"Job")

    val spark = SparkSession
      .builder()
      .appName(this.getClass().getSimpleName)
      .master("local[2]")
      .getOrCreate()

    val jdbcDF = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://cm01:3306/test?useSSL=false")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("dbtable", "travel")
      .option("user", "root")
      .option("password", "123")
      .load().cache()

    jdbcDF.createOrReplaceTempView("obd_TravelInfo")

    spark.udf.register("limitYmdTime", limitYmdTime _)
    spark.udf.register("limitHmsTime", limitHmsTime _)
    spark.udf.register("diffTimeByTimestamp", diffTimeByTimestamp _)

    val sql:String =
      s"""
         |select
         |  ObjectID,
         |  SUM(diffTimeByTimestamp(StartTime,StopTime,'${eveningPeakBeginTimeStr}','${eveningPeakEndTimeStr}')) diffTime
         |from
         |  obd_TravelInfo
         |where
         |  limitYmdTime(StartTime,'${monthBeginDateStr}','${monthEndDateStr}') == true
         |and
         |  limitYmdTime(StopTime,'${monthBeginDateStr}','${monthEndDateStr}') == true
         |and
         |  datediff(StopTime,StartTime) == 0
         |group by
         |  ObjectID
         |""".stripMargin

    logWarning(this.getClass.getSimpleName + "Execute Start Time : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S").format(new Date))
    spark.sql(sql).show(1000,false)
    logWarning(this.getClass.getSimpleName + "Execute End Time : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S").format(new Date))

    spark.close()
  }

}
