package spark.mgworkplace.job

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession
import spark.mgworkplace.job.param.ParamKit._
import spark.mgworkplace.job.udf.UdfKit._

/**
  *
  * @author com.ymy.hadoop   
  * @since 2019/6/16 9:20
  */
object StatNightDrivingTimeJob extends Logging with Job {

  def main(args: Array[String]): Unit = {

    logWarning("开始加载: "+ this.getClass.getSimpleName)

    val spark = SparkSession
      .builder()
      .appName(this.getClass().getSimpleName)
      .master("local[2]")
      .getOrCreate()

    val jdbcDF = spark.read
      .format("jdbc")
      .option("url", "jdbc:mysql://cm01:3306/test?useSSL=false")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("dbtable", "travel")
      .option("user", "root")
      .option("password", "123")
      .load().cache()

    jdbcDF.createOrReplaceTempView("obd_TravelInfo")
    spark.udf.register("limitYmdTime", limitYmdTime _)
    spark.udf.register("diffTimeByTimestamp", diffTimeByTimestamp _)
    spark.udf.register("handleNightDrivingTime", handleNightDrivingTime _)

    logWarning(this.getClass.getSimpleName + "Execute Start Time : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S").format(new Date))
    jdbcDF
      .filter("datediff(StopTime,StartTime) <= 1")
      .filter(s"limitYmdTime(StartTime,'${monthBeginDateStr}','${monthEndDateStr}')")
      .filter(s"limitYmdTime(StopTime,'${monthBeginDateStr}','${monthEndDateStr}')")
      .selectExpr("ObjectID","StartTime","StopTime", s"handleNightDrivingTime(StartTime,StopTime)").show(1000,false)
    logWarning(this.getClass.getSimpleName + "Execute End Time : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:S").format(new Date))

    spark.close()
  }
}
