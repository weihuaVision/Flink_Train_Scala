package spark.mgworkplace.job

import org.apache.hadoop.conf.Configuration
import org.apache.spark.internal.Logging
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}

/**
  *
  * @author eureka.wh
  * @since 2019-06-05 11:31
  */
trait Job extends Serializable with Logging{

  val input = ""

  // 模板方法，子类可以选择自己实现，或者不实现
  def getStruct(): StructType = {
    StructType(Array(
      StructField("id", IntegerType),
      StructField("name", StringType)
    ))
  }

  // 模板方法，子类可以选择自己实现，或者不实现
  def parseLog(log: String): Row = {
    Row(0)
  }

  // 模板方法，子类可以选择自己实现，或者不实现，如果需要子类强制实现，则当前类不实现该发
  def etl(carbonCtx: SparkSession, configuration: Configuration)={

    val catalog = carbonCtx.catalog
    catalog.listDatabases().show(false)
    catalog.listTables("default").show(false)
    catalog.listTables("default").select("name","database").show(false)

    // TODO: ETL

    // 方式一：
//    carbonCtx.sql("")
//
//    // 方式二：
//    val logDF = carbonCtx.read.format("text").load(input)
//    var rowRDD = carbonCtx.sparkContext.makeRDD(Seq(1,"eureka.wh"))
//    carbonCtx.createDataFrame(logDF.rdd.map(x=>{
//      Row(0)
//    }),getStruct())

    carbonCtx.stop()
  }


}
