package spark.mgworkplace.job.param

/**
  *
  * @author com.ymy.hadoop   
  * @since 2019/6/16 9:48
  */
object ParamKit {

  /*
  * 下面参数，可以通过spark-submit参数进行传递，如果未传递，则采用默认值
  * */
  // 月总里程统计
  val monthBeginDateStr = "2019-05-01 00:00:00"
  val monthEndDateStr = "2019-05-31 23:59:59"
  // 早高峰行车时间统计
  val morningPeakBeginTimeStr = "07:00:00"
  val morningPeakEndTimeStr = "10:00:00"
  // 晚高峰行车时间统计
  val eveningPeakBeginTimeStr = "18:00:00"
  val eveningPeakEndTimeStr = "20:00:00"
 // 夜间行车时间统计
  val nightDrivingBeginTimeStr = "21:00:00"
  val nightDrivingSeparationTimeStr = "23:59:59"
  val nightDrivingEndTimeStr = "07:00:00"
}
