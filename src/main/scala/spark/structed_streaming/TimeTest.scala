package spark.structed_streaming

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.Date



/**
  *
  * @author eureka.wh
  * @since 2019-06-10 15:21
  */
object TimeTest {

  def main(args: Array[String]): Unit = {


    val str = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date)

     println(getTimestamp(str))

  }


  def getTimestamp(x:String) :java.sql.Timestamp = {

    val format = new SimpleDateFormat("yyyyMMddHHmmss")
    var ts = new Timestamp(System.currentTimeMillis())

    try{
      if(x == ""){
        return null
      }else{
        val d = format.parse(x);
        val t = new Timestamp(d.getTime());
        return t;
      }
    }catch{
      case e: Exception => println("parse timestamp wrong")
    }
    return null
  }


}
