package spark.RDD

import org.apache.spark.sql.SparkSession

/**
  *
  * @author com.eureka.wh   
  * @since 2019/6/23 0:14
  */
object SparkRDD01 {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder()
      .appName(this.getClass().getSimpleName)
      .master("local[2]")
      .getOrCreate()

    val WORDS = Array[String]("To be, or not to be,--that is the question:--", "Whether 'tis nobler in the mind to suffer", "The slings and arrows of outrageous fortune", "And by opposing end them?--To die,--to sleep,--", "Be all my sins remember'd.")

    spark.sparkContext.parallelize(WORDS).flatMap(x=>x.split(" ")).foreach(println)

    spark.close()

  }
}
