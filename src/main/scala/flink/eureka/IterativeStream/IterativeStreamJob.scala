package flink.eureka.IterativeStream

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  *
  * @author com.eureka.wh   
  * @since 2019/6/22 22:51
  */
object IterativeStreamJob {

  def main(args: Array[String]): Unit = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val input = env.generateSequence(0,100)

    input.print()

    env.execute("IterativeStreamJob")
  }
}
