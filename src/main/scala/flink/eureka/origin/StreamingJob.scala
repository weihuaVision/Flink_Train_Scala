/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package flink.eureka.origin

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._

/**
 * Skeleton for a Flink Streaming Job.
 *
 * For a tutorial how to write a Flink streaming application, check the
 * tutorials and examples on the <a href="http://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * To package your application into a JAR file for execution, run
 * 'mvn clean package' on the command line.
 *
 * If you change the name of the main class (with the public static void main(String[] args))
 * method, change the respective entry in the POM.xml file (simply search for 'mainClass').
 */
object StreamingJob {

  def main(args: Array[String]) {
    // set up the streaming execution environment
//    val env = StreamExecutionEnvironment.getExecutionEnvironment
//
//    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

//    val env = ExecutionEnvironment.getExecutionEnvironment

    //    env.fromCollection(List(new Person("xx",12)))
//    val ds = env.fromCollection(new Person("xx",12)
//                      ::new Person("yy",22)
//                      ::new Person("yy",22)
//                      ::Nil)
//    val ds = env.fromElements(("xx",12)
//      ,("yy",122)
//      ,("yy",422)
//      )
//    ds.groupBy("_1").max(1).print()
    // ERROR : Aggregating on field positions is only possible on tuple data types.
//    val ds = env.fromElements(new Person("xx",12)
//      ,new Person("yy",122)
//      ,new Person("yy",422)
//    )
//
//    ds.groupBy("name").max(1).print()

    // execute program
//    env.execute("Flink Streaming Scala API Skeleton")
  }
}

