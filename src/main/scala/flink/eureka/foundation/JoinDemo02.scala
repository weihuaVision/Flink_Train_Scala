package flink.eureka.foundation

import org.apache.flink.api.scala.ExecutionEnvironment

import scala.collection.mutable.ListBuffer

import org.apache.flink.api.scala._
/**
  *
  * @author com.ymy.hadoop   
  * @since 2019/6/4 0:20
  */
object JoinDemo02 {

  def main(args: Array[String]): Unit = {

    val env = ExecutionEnvironment.getExecutionEnvironment

    val info1 = ListBuffer[(Demo)]()  // 编号  名字
    info1.append((new Demo(1, "PK哥")));
    info1.append((new Demo(2, "J哥")));
    info1.append((new Demo(3, "小队长")));
    info1.append((new Demo(4, "猪头呼")));

    val info2 = ListBuffer[(Demo)]()  // 编号  城市
    info1.append((new Demo(1, "PK哥")));
    info1.append((new Demo(2, "J哥")));
    info1.append((new Demo(3, "小队长")));
//    info1.append((4, new Demo(4, "猪头呼")));

      val df1 = env.fromCollection(info1)
      val df2 = env.fromCollection(info2)

      df1.join(df2).where("num").equalTo("num").print()

  }
}

class Demo(num: Int,name: String){
  def this(){
    this(-1,null)
  }
}
