package flink.eureka.foundation

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment

/**
  *
  * @author com.ymy.hadoop   
  * @since 2019/6/4 0:01
  */
object PoJoDemo01 {

  def main(args: Array[String]): Unit = {
      val env = StreamExecutionEnvironment.getExecutionEnvironment
      val ds = env.fromElements(new Person("xxx",14),new Person("jjj",15))
    ds.keyBy("name").max(1).print()

    val ds2 = env.fromElements(new Person("xxx",94),new Person("jjj2",95))

    import org.apache.flink.api.scala._

    // 下面where会报错，是因为join后是需要tuple类型的datastream
//    ds.join(ds2).where("name")

  }
}
