package flink.eureka.foundation

/**
  *
  * @author com.ymy.hadoop   
  * @since 2019/6/4 0:02
  */
class Person(var name:String,var age:Int) {
  // 默认空的构造器
  def this(){
    this(null,-1)
  }
}
