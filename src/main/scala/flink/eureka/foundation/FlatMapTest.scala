package flink.eureka.foundation

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  *
  * @author com.eureka.wh   
  * @since 2019/6/23 0:08
  */
object FlatMapTest {

  def main(args: Array[String]): Unit = {

    //  引入隐式转换，下面使用了flatMap，注意要导入隐式转换！！！！，否则会报错
    import org.apache.flink.api.scala._

    val WORDS = List("1,1,1","2,2,2","3,3,3")

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    env.fromCollection(WORDS).flatMap(x=>x.split(",")).print();

    env.execute("FlatMapTest")
  }
}
