package flink.eureka.connector

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

object StreamingWCScalaApp {

  def main(args: Array[String]): Unit = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    // 引入隐式转换
    import org.apache.flink.api.scala._

    val text = env.socketTextStream("localhost", 9999)

    val counts = text.flatMap(_.split(","))
      .filter(_.nonEmpty)
      .map((_, 1))
      .keyBy(0)
      .sum(1)
      .setParallelism(1)

    counts.print()

    env.execute("StreamingWCScalaApp")
  }

}
