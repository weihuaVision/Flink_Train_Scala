package flink.eureka.connector

import org.apache.flink.api.scala.ExecutionEnvironment

object BatchWCScalaApp {

  def main(args: Array[String]): Unit = {

    val input = "file:///opt/module/flink-1.8.0/hadoop2.6.0-cdh5.14.2/flink-1.8.0/wc.txt"

    val env = ExecutionEnvironment.getExecutionEnvironment

    val text = env.readTextFile(input)

    // 引入隐式转换
    import org.apache.flink.api.scala._

    // TODO... 1) 参考Scala课程  2）API再来讲
    text.flatMap(_.toLowerCase.split("\t"))
      .filter(_.nonEmpty)
      .map((_,1))
      .groupBy(0)
      .sum(1).print()

//    env.execute("BatchWCScalaApp")
  }
}
